# Quake Ⅲ Arena map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Quake Ⅲ Arena.

This gamepack is based on the game pack provided by NetRadiant-custom.

More stuff may be imported from http://svn.icculus.org/gtkradiant-gamepacks/Q3Pack/ in the future.

Required data samples are not included.
